# Flight Advisor

- Run as Spring Boot app
- All endpoints are exposed on **localhost:8080/api/**
- **H2** is used as in-memory database
- JWT token expiry time can be changed in **application.properties**. Default is *1 hour*.
- Extending expiry time of JWT on every request can be changed in **application.properties**. Default is *false*.
---
- Admin user is created on startup (username: admin, password: admin)
- There is also an exposed endpoint for admin registration if needed: */api/users/register-admin*
---
- The algorithm used for finding cheapest routes is **Dijkstra's SPF algorithm**. The java implementation used for reference can be found on link https://www.baeldung.com/java-dijkstra 