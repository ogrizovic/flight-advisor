package com.htec.flightadvisor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.htec.flightadvisor.auth.JWTAuthenticationFilter;
import com.htec.flightadvisor.auth.JWTAuthorizationFilter;
import com.htec.flightadvisor.auth.user.service.UserService;

@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    public static final String PEPPER = "SomePepperHereAndThere";
    public static final String SECRET = "ThisShouldBeLonger";
    public static final String BEARER = "Bearer ";

    public static Long EXPIRY_MILLIS = 3600000L;
    public static boolean EXTEND_TOKEN = false;

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    public SecurityConfig(@Value("${flightadvisor.security.token-expiry-millis}") Long expiryMillis,
                          @Value("${flightadvisor.security.extend-token}") boolean extendToken) {
        super();
        EXPIRY_MILLIS = expiryMillis;
        EXTEND_TOKEN = extendToken;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable()
                .authorizeRequests()
                .antMatchers("/importdata").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST,"/cities").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST, "/users/register").permitAll()
                .anyRequest().authenticated()
                .and()
                .addFilter(getJWTAuthenticationFilter())
                .addFilter(getJwtAuthorizationFilter());
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService).passwordEncoder(passwordEncoder);
    }

    private JWTAuthenticationFilter getJWTAuthenticationFilter() throws Exception {
        final JWTAuthenticationFilter filter = new JWTAuthenticationFilter(authenticationManager());
        filter.setFilterProcessesUrl("/login");
        return filter;
    }

    private JWTAuthorizationFilter getJwtAuthorizationFilter() throws Exception {
        return new JWTAuthorizationFilter(authenticationManager(), userService);
    }
}
