package com.htec.flightadvisor.auth;

import java.io.IOException;
import java.sql.Date;
import java.time.Instant;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.htec.flightadvisor.SecurityConfig;

import static com.auth0.jwt.algorithms.Algorithm.HMAC512;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

    private final UserDetailsService userDetailsService;

    public JWTAuthorizationFilter(AuthenticationManager authenticationManager, UserDetailsService userDetailsService) {
        super(authenticationManager);
        this.userDetailsService = userDetailsService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest req,
                                    HttpServletResponse res,
                                    FilterChain chain) throws IOException, ServletException {
        String header = req.getHeader(HttpHeaders.AUTHORIZATION);

        if (header == null || !header.startsWith(SecurityConfig.BEARER)) {
            chain.doFilter(req, res);
            return;
        }

        UsernamePasswordAuthenticationToken authentication = getAuthentication(req, res);

        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(req, res);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request, HttpServletResponse response) {
        String token = request.getHeader(HttpHeaders.AUTHORIZATION);
        if (token != null) {
            String username = JWT.require(Algorithm.HMAC512(SecurityConfig.SECRET))
                    .build()
                    .verify(token.replace(SecurityConfig.BEARER, ""))
                    .getSubject();

            UserDetails loadedUser = userDetailsService.loadUserByUsername(username);
            if (loadedUser != null) {

                if (SecurityConfig.EXTEND_TOKEN) {
                    extendToken(loadedUser.getUsername(), response);
                }

                return new UsernamePasswordAuthenticationToken(loadedUser.getUsername(), null, loadedUser.getAuthorities());
            }
            return null;
        }
        return null;
    }

    private void extendToken(String username, HttpServletResponse response) {
        Instant expiresAt = Instant.now().plusMillis(SecurityConfig.EXPIRY_MILLIS);
        String token = JWT.create()
                .withSubject(username)
                .withExpiresAt(Date.from(expiresAt))
                .sign(HMAC512(SecurityConfig.SECRET));
        response.addHeader(HttpHeaders.AUTHORIZATION, SecurityConfig.BEARER + token);
    }
}
