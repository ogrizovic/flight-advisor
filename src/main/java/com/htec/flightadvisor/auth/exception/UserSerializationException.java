package com.htec.flightadvisor.auth.exception;

public class UserSerializationException extends RuntimeException {

    private static final String DEFAULT_MSG = "User serialization failed.";

    public UserSerializationException(Throwable e) {
        super(DEFAULT_MSG, e);
    }
}
