package com.htec.flightadvisor.auth.exception;

public class UsernameTakenException extends RuntimeException {

    private static final String DEFAULT_MSG = "Username is taken. Try a different one.";

    public UsernameTakenException() {
        super(DEFAULT_MSG);
    }
}
