package com.htec.flightadvisor.auth.model;

import lombok.Data;

@Data
public class LoginDto {
    private String username;
    private String password;
}
