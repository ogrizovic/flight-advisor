package com.htec.flightadvisor.auth.model;

import java.util.Optional;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.security.core.GrantedAuthority;

import com.htec.flightadvisor.auth.enums.RoleName;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter @Setter
public class Role implements GrantedAuthority {

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Enumerated(EnumType.STRING)
    private RoleName name = RoleName.USER;

    @Override
    public String getAuthority() {
        return Optional.ofNullable(this.name)
                .map(RoleName::getName)
                .orElse("");
    }
}
