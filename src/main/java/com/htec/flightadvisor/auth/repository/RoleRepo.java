package com.htec.flightadvisor.auth.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.htec.flightadvisor.auth.enums.RoleName;
import com.htec.flightadvisor.auth.model.Role;

public interface RoleRepo extends JpaRepository<Role, Integer> {
    Role findOneByName(RoleName name);
}
