package com.htec.flightadvisor.auth.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.htec.flightadvisor.auth.user.model.AppUser;
import com.htec.flightadvisor.auth.user.model.UserDto;
import com.htec.flightadvisor.auth.user.service.UserService;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/register")
    public ResponseEntity<UserDto> register(@RequestBody AppUser newUser) {
        AppUser user = userService.register(newUser);
        return new ResponseEntity<>(new UserDto(user), HttpStatus.CREATED);
    }

    @PostMapping("/register-admin")
    public ResponseEntity<UserDto> registerAdmin(@RequestBody AppUser newUser) {
        AppUser user = userService.registerAdmin(newUser);
        return new ResponseEntity<>(new UserDto(user), HttpStatus.CREATED);
    }
}
