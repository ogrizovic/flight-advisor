package com.htec.flightadvisor.auth.user.model;

import lombok.Data;

@Data
public class UserDto {

    private String firstName;
    private String lastName;
    private String username;

    public UserDto(AppUser entity) {
        this.firstName = entity.getFirstName();
        this.lastName = entity.getLastName();
        this.username = entity.getUsername();
    }

}
