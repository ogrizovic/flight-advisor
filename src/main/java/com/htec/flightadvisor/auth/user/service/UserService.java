package com.htec.flightadvisor.auth.user.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.htec.flightadvisor.auth.user.model.AppUser;

public interface UserService extends UserDetailsService {

    AppUser register(AppUser user);
    AppUser registerAdmin(AppUser user);
    String getLoggedInUser();
}
