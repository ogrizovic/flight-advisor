package com.htec.flightadvisor.auth.user.service;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.htec.flightadvisor.SecurityConfig;
import com.htec.flightadvisor.auth.enums.RoleName;
import com.htec.flightadvisor.auth.exception.UsernameTakenException;
import com.htec.flightadvisor.auth.repository.RoleRepo;
import com.htec.flightadvisor.auth.user.model.AppUser;
import com.htec.flightadvisor.auth.user.repository.UserRepo;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepo userRepo;
    private final PasswordEncoder passwordEncoder;
    private final RoleRepo roleRepo;

    @Autowired
    public UserServiceImpl(UserRepo userRepo, PasswordEncoder passwordEncoder, RoleRepo roleRepo) {
        this.userRepo = userRepo;
        this.passwordEncoder = passwordEncoder;
        this.roleRepo = roleRepo;
    }

    @PostConstruct
    private void addAdmin() {
        AppUser user = new AppUser();
        user.setFirstName("Admin");
        user.setLastName("Admin");
        user.setUsername("admin");
        user.setPassword("admin");
        registerAdmin(user);
    }

    @Override
    public AppUser register(AppUser user) {
        if (user.getUsername() == null) {
            throw new UsernameNotFoundException(user.getUsername());
        }
        if (userRepo.findByUsername(user.getUsername()).isPresent()) {
            throw new UsernameTakenException();
        }

        user.setPassword(passwordEncoder.encode(SecurityConfig.PEPPER + user.getPassword()));
        user.setRole(roleRepo.findOneByName(RoleName.USER));
        return userRepo.save(user);
    }

    @Override
    public AppUser registerAdmin(AppUser user) {
        user.setPassword(passwordEncoder.encode(SecurityConfig.PEPPER + user.getPassword()));
        user.setRole(roleRepo.findOneByName(RoleName.ADMIN));
        return userRepo.save(user);
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        return userRepo.findByUsername(username)
                .map(user -> new User(user.getUsername(), user.getPassword(), user.getAuthorities()))
                .orElseThrow(() -> new UsernameNotFoundException(username));
    }

    @Override
    public String getLoggedInUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth.getName();
    }
}
