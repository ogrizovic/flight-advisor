package com.htec.flightadvisor.city.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.htec.flightadvisor.city.model.City;
import com.htec.flightadvisor.city.service.CityService;
import com.htec.flightadvisor.comment.model.Comment;
import com.htec.flightadvisor.comment.service.CommentService;

@RestController
@RequestMapping("/cities")
public class CityController {

    private final CityService cityService;
    private final CommentService commentService;

    @Autowired
    public CityController(CityService cityService, CommentService commentService) {
        this.cityService = cityService;
        this.commentService = commentService;
    }

    @GetMapping
    public ResponseEntity<List<City>> getAll(@RequestParam(name = "last", required = false) Integer last) {
        List<City> cities;
        if (last == null) {
            cities = cityService.getAll();
        } else {
            cities = cityService.getAll(last);
        }
        return new ResponseEntity<>(cities, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<City> add(@RequestBody City city) {
        return new ResponseEntity<>(cityService.add(city), HttpStatus.CREATED);
    }

    @GetMapping("/{name}")
    public ResponseEntity<City> getOne(@PathVariable String name, @RequestParam(name = "last", required = false) Integer last) {
        City city;
        if (last == null) {
            city = cityService.getOneByName(name);
        } else {
            city = cityService.getOneByName(name, last);
        }
        return new ResponseEntity<>(city, HttpStatus.OK);
    }


    @PostMapping("/{cityId}/comments")
    public ResponseEntity<City> comment(@PathVariable("cityId") Long cityId, @RequestBody Comment comment) {
        return new ResponseEntity<>(cityService.comment(cityId, comment), HttpStatus.CREATED);
    }

    @PutMapping("/{cityId}/comments")
    public ResponseEntity<Comment> editComment(@RequestBody Comment comment) {
        return new ResponseEntity<>(commentService.update(comment), HttpStatus.OK);
    }

    @DeleteMapping("/{cityId}/comments/{commentId}")
    public ResponseEntity<Void> deleteComment(@PathVariable("commentId") Long commentId) {
        commentService.delete(commentId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
