package com.htec.flightadvisor.city.exception;

public class CityNotFoundException extends RuntimeException {

    public CityNotFoundException(String msg) {
        super(msg);
    }
}
