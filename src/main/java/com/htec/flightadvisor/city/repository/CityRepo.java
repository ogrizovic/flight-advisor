package com.htec.flightadvisor.city.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.htec.flightadvisor.city.model.City;

@Repository
public interface CityRepo extends JpaRepository<City, Long> {

    City findOneByName(String name);
}
