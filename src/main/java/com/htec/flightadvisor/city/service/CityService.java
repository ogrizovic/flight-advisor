package com.htec.flightadvisor.city.service;

import java.util.List;

import com.htec.flightadvisor.city.model.City;
import com.htec.flightadvisor.comment.model.Comment;

public interface CityService {

    City add(City city);
    List<City> getAll();
    City getOneByName(String name);
    City getOneByName(String name, Integer lastComments);
    City comment(Long cityId, Comment comment);
    List<String> getAddedCityNames();
    List<City> getAll(Integer lastComments);
}
