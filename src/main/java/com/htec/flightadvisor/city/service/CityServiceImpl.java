package com.htec.flightadvisor.city.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.htec.flightadvisor.auth.user.service.UserService;
import com.htec.flightadvisor.city.exception.CityNotFoundException;
import com.htec.flightadvisor.city.model.City;
import com.htec.flightadvisor.city.repository.CityRepo;
import com.htec.flightadvisor.comment.model.Comment;
import com.htec.flightadvisor.comment.repository.CommentRepo;

@Service
public class CityServiceImpl implements CityService {

    private final CityRepo cityRepo;
    private final UserService userService;
    private final CommentRepo commentRepo;

    @Autowired
    public CityServiceImpl(CityRepo cityRepo, UserService userService, CommentRepo commentRepo) {
        this.cityRepo = cityRepo;
        this.userService = userService;
        this.commentRepo = commentRepo;
    }

    @Override
    public City add(City city) {
        return cityRepo.save(city);
    }

    @Override
    public List<City> getAll() {
        return cityRepo.findAll();
    }

    @Override
    public List<City> getAll(Integer lastComments) {
        Pageable pageable = PageRequest.of(0, lastComments, Sort.Direction.DESC, "created");
        List<City> cities = cityRepo.findAll();
        cities.forEach(city -> {
            Page<Comment> page = commentRepo.findByCityId(city.getId(), pageable);
            city.setComments(page.getContent());
        });

        return cities;
    }

    @Override
    public City getOneByName(String name) {
        return cityRepo.findOneByName(name);
    }

    @Override
    public City getOneByName(String name, Integer lastComments) {
        Pageable pageable = PageRequest.of(0, lastComments, Sort.Direction.DESC, "created");
        City city = cityRepo.findOneByName(name);
        if (city != null) {
            Page<Comment> page = commentRepo.findByCityId(city.getId(), pageable);
            city.setComments(page.getContent());
        }
        return city;
    }

    @Override
    public City comment(Long cityId, Comment comment) {
        City city = cityRepo.getOne(cityId);
        if (city == null) {
            throw new CityNotFoundException(cityId.toString());
        }
        comment.setCreatedBy(userService.getLoggedInUser());
        city.addComment(comment);

        return cityRepo.save(city);
    }

    @Override
    public List<String> getAddedCityNames() {
        return getAll().stream()
                .map(City::getName)
                .collect(Collectors.toList());
    }
}
