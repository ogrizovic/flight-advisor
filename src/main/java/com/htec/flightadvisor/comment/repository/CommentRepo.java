package com.htec.flightadvisor.comment.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.htec.flightadvisor.comment.model.Comment;

public interface CommentRepo extends PagingAndSortingRepository<Comment, Long> {
    Page<Comment> findByCityId(Long cityId, Pageable pageable);
}
