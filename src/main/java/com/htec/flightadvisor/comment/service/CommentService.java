package com.htec.flightadvisor.comment.service;

import com.htec.flightadvisor.comment.model.Comment;

public interface CommentService {

    Comment update(Comment comment);
    void delete(Long id);
}
