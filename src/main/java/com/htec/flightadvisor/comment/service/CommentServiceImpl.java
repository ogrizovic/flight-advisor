package com.htec.flightadvisor.comment.service;

import java.time.Instant;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.htec.flightadvisor.auth.user.service.UserService;
import com.htec.flightadvisor.comment.model.Comment;
import com.htec.flightadvisor.comment.repository.CommentRepo;

@Service
public class CommentServiceImpl implements CommentService {

    private final CommentRepo commentRepo;
    private final UserService userService;

    @Autowired
    public CommentServiceImpl(CommentRepo commentRepo, UserService userService) {
        this.commentRepo = commentRepo;
        this.userService = userService;
    }

    @Override
    public Comment update(Comment comment) {
        comment.setModified(Instant.now());
        return commentRepo.save(comment);
    }

    @Override
    public void delete(Long id) {
        Optional<Comment> comment = commentRepo.findById(id);
        comment.ifPresent(com -> {
            if (com.getCreatedBy().equals(userService.getLoggedInUser())) {
                commentRepo.deleteById(id);
            }
        });
    }
}
