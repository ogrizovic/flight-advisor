package com.htec.flightadvisor.importdata.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.htec.flightadvisor.importdata.service.ImportDataService;

@RestController
@RequestMapping("/importdata")
public class ImportDataController {

    private final ImportDataService importDataService;

    @Autowired
    public ImportDataController(ImportDataService importDataService) {
        this.importDataService = importDataService;
    }

    @GetMapping
    public ResponseEntity<Void> importData() {
        importDataService.importData();
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
