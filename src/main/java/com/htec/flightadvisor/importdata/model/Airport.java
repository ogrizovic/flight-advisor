package com.htec.flightadvisor.importdata.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.apache.commons.lang3.math.NumberUtils;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter @EqualsAndHashCode @NoArgsConstructor
public class Airport {

    @Id
    private Integer airportId;
    private String name;
    private String cityName;
    private String country;
    @Column(length = 3)
    private String iata;
    @Column(length = 4)
    private String icao;
    private Double latitude;
    private Double longitude;
    private Integer altitudeInFeet;
    private Integer timezoneOffset;
    private String dst;
    private String tz;
    private String type;
    private String sourceOfData;

    public Airport(String[] airport) {
        this.airportId = Integer.valueOf(airport[0]);
        this.name = airport[1];
        this.cityName = airport[2];
        this.country = airport[3];
        this.iata = airport[4];
        this.icao = airport[5];
        this.latitude = NumberUtils.isCreatable(airport[6]) ? Double.parseDouble(airport[6]) : -1;
        this.longitude = NumberUtils.isCreatable(airport[7]) ? Double.parseDouble(airport[7]) : -1;
        this.altitudeInFeet = NumberUtils.isCreatable(airport[8]) ? Integer.parseInt(airport[8]) : -1;
        this.timezoneOffset = NumberUtils.isCreatable(airport[9]) ? Integer.parseInt(airport[9]) : 0;
        this.dst = airport[10];
        this.tz = airport[11];
        this.type = airport[12];
        this.sourceOfData = airport[13];
    }
}
