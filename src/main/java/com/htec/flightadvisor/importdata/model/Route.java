package com.htec.flightadvisor.importdata.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.apache.commons.lang3.math.NumberUtils;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter @Setter @EqualsAndHashCode @NoArgsConstructor
public class Route {

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(length = 3)
    private String airline;
    private Integer airlineId;
    @Column(length = 4)
    private String sourceAirport;
    private Integer sourceAirportId;
    @Column(length = 4)
    private String destinationAirport;
    private Integer destinationAirportId;
    private String codeshare;
    private Integer stops;
    private String equipment;
    private Double price;

    public Route(String[] route) {
        this.airline = route[0];
        this.airlineId = NumberUtils.isCreatable(route[1]) ? Integer.parseInt(route[1]) : -1;
        this.sourceAirport = route[2];
        this.sourceAirportId = NumberUtils.isCreatable(route[3]) ? Integer.parseInt(route[3]) : -1;
        this.destinationAirport = route[4];
        this.destinationAirportId = NumberUtils.isCreatable(route[5]) ? Integer.parseInt(route[5]) : -1;
        this.codeshare = route[6];
        this.stops = NumberUtils.isCreatable(route[7]) ? Integer.parseInt(route[7]) : -1;
        this.equipment = route[8];
        this.price = NumberUtils.isCreatable(route[9]) ? Double.parseDouble(route[9]) : Double.MAX_VALUE;

    }
//    @ManyToOne
//    @JoinColumn(name = "airport_id")
//    private Airport sourceAirport;


}
