package com.htec.flightadvisor.importdata.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.htec.flightadvisor.importdata.model.Airport;

@Repository
public interface AirportRepo extends JpaRepository<Airport, Integer> {

    List<Airport> findByCityName(String cityName);
}
