package com.htec.flightadvisor.importdata.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.htec.flightadvisor.importdata.model.Route;

@Repository
public interface RouteRepo extends JpaRepository<Route, Long> {
    Route findBySourceAirportIdAndDestinationAirportIdAndPrice(Integer source, Integer destination, Double price);
}
