package com.htec.flightadvisor.importdata.service;

import java.util.List;

import com.htec.flightadvisor.importdata.model.Airport;

public interface AirportService {
    List<Airport> getAll();
    Airport add(Airport airport);
    List<Airport> addAll(List<Airport> airports);
    Airport getById(Integer id);
    List<Airport> getByCityName(String cityName);
    List<Integer> getAirportIdsForCity(String cityName);
}
