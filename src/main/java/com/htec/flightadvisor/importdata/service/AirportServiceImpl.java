package com.htec.flightadvisor.importdata.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.htec.flightadvisor.importdata.model.Airport;
import com.htec.flightadvisor.importdata.repository.AirportRepo;

@Service
public class AirportServiceImpl implements AirportService {

    private final AirportRepo airportRepo;

    @Autowired
    public AirportServiceImpl(AirportRepo airportRepo) {
        this.airportRepo = airportRepo;
    }

    @Override
    public List<Airport> getAll() {
        return airportRepo.findAll();
    }

    @Override
    public Airport add(Airport airport) {
        return airportRepo.save(airport);
    }

    @Override
    public List<Airport> addAll(List<Airport> airports) {
        return airportRepo.saveAll(airports);
    }

    @Override
    public List<Airport> getByCityName(String cityName) {
        return airportRepo.findByCityName(cityName);
    }

    @Override
    public Airport getById(Integer id) {
        return airportRepo.getOne(id);
    }

    @Override
    public List<Integer> getAirportIdsForCity(String cityName) {
        List<Airport> airport = getByCityName(cityName);
        return airport.stream().map(Airport::getAirportId).collect(Collectors.toList());
    }
}
