package com.htec.flightadvisor.importdata.service;

public interface ImportDataService {

    void importData();
}
