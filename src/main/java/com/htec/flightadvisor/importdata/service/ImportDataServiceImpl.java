package com.htec.flightadvisor.importdata.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import com.htec.flightadvisor.city.service.CityService;
import com.htec.flightadvisor.importdata.model.Airport;
import com.htec.flightadvisor.importdata.model.Route;

@Service
public class ImportDataServiceImpl implements ImportDataService {

    private final CityService cityService;
    private final AirportService airportService;
    private final RouteService routeService;

    @Autowired
    public ImportDataServiceImpl(CityService cityService, AirportService airportService, RouteService routeService) {
        this.cityService = cityService;
        this.airportService = airportService;
        this.routeService = routeService;
    }

    @Override
    public void importData() {
        List<String> addedCities = cityService.getAddedCityNames();
        List<Airport> airports = importAirportsForAddedCities(addedCities);
        airportService.addAll(airports);

        List<Route> routes = importRoutesForAirports(airports);
        routeService.deleteAll();
        routeService.addAll(routes);
    }


    private List<Route> importRoutesForAirports(List<Airport> airports) {

        List<Route> routes = new ArrayList<>();
        BufferedReader bReader;
        String line = "";

        Map<Integer, Airport> airportIds = airports.stream()
                .collect(Collectors.toMap(Airport::getAirportId, Function.identity()));

        try {
            File routesFile = ResourceUtils.getFile("classpath:datasets\\routes.txt");

            bReader = new BufferedReader(new FileReader(routesFile));
            while ((line = bReader.readLine()) != null) {

                String [] route = unqoteArray(line.split(","));


                if (shouldImport(airportIds, route[3], route[5])) {
                    routes.add(new Route(route));
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return routes;
    }

    private boolean shouldImport(Map<Integer, Airport> airportIds, String source, String destination) {

        if (NumberUtils.isCreatable(source) && NumberUtils.isCreatable(destination)) {
            return airportIds.containsKey(Integer.parseInt(source)) && airportIds.containsKey(Integer.parseInt(destination));
        }

        return false;
    }

    private List<Airport> importAirportsForAddedCities(List<String> addedCities) {

        List<Airport> airports = new ArrayList<>();
        BufferedReader bReader;
        String line = "";

        try {
            File airportsFile = ResourceUtils.getFile("classpath:datasets\\airports.txt");

            bReader = new BufferedReader(new FileReader(airportsFile));
            while ((line = bReader.readLine()) != null) {

                String [] airport = unqoteArray(line.split(","));

                if (addedCities.contains(airport[2])) {
                    airports.add(new Airport(airport));
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return airports;
    }

    private String unqote(String s) {
        return s.replaceAll("^\"|\"$", "");
    }

    private String[] unqoteArray(String[] sa) {
        String [] unqotedArray = new String[sa.length];
        for (int i = 0; i < sa.length; i++) {
            unqotedArray[i] = unqote(sa[i]);
        }
        return unqotedArray;
    }
}
