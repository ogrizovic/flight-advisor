package com.htec.flightadvisor.importdata.service;

import java.util.List;

import com.htec.flightadvisor.importdata.model.Route;

public interface RouteService {
    List<Route> getAll();
    List<Route> addAll(List<Route> airports);
    void deleteAll();
    Route getBySourceDestPrice(Integer sourceAirportId, Integer destinationAirportId, Double price);
}
