package com.htec.flightadvisor.importdata.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.htec.flightadvisor.importdata.model.Route;
import com.htec.flightadvisor.importdata.repository.RouteRepo;

@Service
public class RouteServiceImpl implements RouteService {

    private final RouteRepo routeRepo;

    @Autowired
    public RouteServiceImpl(RouteRepo routeRepo) {
        this.routeRepo = routeRepo;
    }

    @Override
    public List<Route> getAll() {
        return routeRepo.findAll();
    }

    @Override
    public List<Route> addAll(List<Route> routes) {
        return routeRepo.saveAll(routes);
    }

    @Override
    public void deleteAll() {
        routeRepo.deleteAll();
    }

    @Override
    public Route getBySourceDestPrice(Integer sourceAirportId, Integer destinationAirportId, Double price) {
        return routeRepo.findBySourceAirportIdAndDestinationAirportIdAndPrice(sourceAirportId, destinationAirportId, price);
    }


}
