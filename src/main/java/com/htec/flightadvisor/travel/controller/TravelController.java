package com.htec.flightadvisor.travel.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.htec.flightadvisor.travel.model.TravelDto;
import com.htec.flightadvisor.travel.service.TravelService;

@RestController
@RequestMapping("/travel")
public class TravelController {

    private final TravelService travelService;

    @Autowired
    public TravelController(TravelService travelService) {
        this.travelService = travelService;
    }

    @GetMapping
    public ResponseEntity<TravelDto> travel(
            @RequestParam("src") String source,
            @RequestParam("dest") String destination) {

        return new ResponseEntity<>(travelService.travel(source, destination), HttpStatus.OK);
    }
}
