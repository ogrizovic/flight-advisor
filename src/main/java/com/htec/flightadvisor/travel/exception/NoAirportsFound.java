package com.htec.flightadvisor.travel.exception;

public class NoAirportsFound extends RuntimeException {

    public NoAirportsFound(String msg) {
        super(msg);
    }
}
