package com.htec.flightadvisor.travel.model;

import com.htec.flightadvisor.importdata.model.Route;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Edge implements IEdge {

    private Integer source;
    private Integer destination;
    private Double cost;

    public Edge(Route route) {
        this.source = route.getSourceAirportId();
        this.destination = route.getDestinationAirportId();
        this.cost = route.getPrice();
    }

}
