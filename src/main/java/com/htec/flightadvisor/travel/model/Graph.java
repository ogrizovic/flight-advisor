package com.htec.flightadvisor.travel.model;

import java.util.HashSet;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@ToString
public class Graph implements IGraph {

    private Set<INode> nodes = new HashSet<>();

    @Override
    public void addNode(INode node) {
        nodes.add(node);
    }
}
