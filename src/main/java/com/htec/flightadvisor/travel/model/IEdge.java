package com.htec.flightadvisor.travel.model;

public interface IEdge {
    Integer getSource();

    Integer getDestination();

    Double getCost();
}
