package com.htec.flightadvisor.travel.model;

public interface IGraph {
    void addNode(INode node);

    java.util.Set<INode> getNodes();

    void setNodes(java.util.Set<INode> nodes);
}
