package com.htec.flightadvisor.travel.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public interface INode extends Serializable {
    void addDestination(INode destination, double distance);

    Integer getName();

    List<INode> getShortestPath();

    Double getDistance();

    Map<INode, Double> getAdjacentNodes();

    void setName(Integer name);

    void setShortestPath(List<INode> shortestPath);

    void setDistance(Double distance);

    void setAdjacentNodes(java.util.Map<INode, Double> adjacentNodes);
}
