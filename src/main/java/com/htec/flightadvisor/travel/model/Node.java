package com.htec.flightadvisor.travel.model;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @EqualsAndHashCode
public class Node implements INode {

    private Integer name;

    private List<INode> shortestPath = new LinkedList<>();

    private Double distance = Double.MAX_VALUE;

    Map<INode, Double> adjacentNodes = new HashMap<>();

    @Override
    public void addDestination(INode destination, double distance) {
        adjacentNodes.put(destination, distance);
    }

    public Node(Integer name) {
        this.name = name;
    }

    public Node(Node node) {
        this.name = node.name;
        this.shortestPath = new LinkedList<>(node.shortestPath);
        this.distance = node.distance;
        this.adjacentNodes = new HashMap<>(node.adjacentNodes);
    }

    @Override
    public String toString() {
        return "Node: [" + name + ", " + distance + "]";
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof Node)) return false;
        Node n = (Node) o;
        return n.getName().equals(this.name);
    }
}
