package com.htec.flightadvisor.travel.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @AllArgsConstructor @NoArgsConstructor
public class RouteDto {
    String sourceCityName;
    String destinationCityName;
    double price;
}
