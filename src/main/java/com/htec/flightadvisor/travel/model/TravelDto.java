package com.htec.flightadvisor.travel.model;

import java.util.List;

import lombok.Data;

@Data
public class TravelDto {
    List<RouteDto> routes;
    double totalPrice;

    public TravelDto(List<RouteDto> routes, double totalPrice) {
        this.routes = routes;
        this.totalPrice = totalPrice;
    }
}
