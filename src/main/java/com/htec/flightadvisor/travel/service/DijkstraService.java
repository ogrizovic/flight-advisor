package com.htec.flightadvisor.travel.service;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.htec.flightadvisor.travel.model.IGraph;
import com.htec.flightadvisor.travel.model.INode;

import lombok.NoArgsConstructor;

@Service @NoArgsConstructor
public class DijkstraService implements PathFindingAlgorithm {

    @Override
    public IGraph findPath(IGraph IGraph, INode source) {
        source.setDistance(0.0);

        Set<INode> settledNodes = new HashSet<>();
        Set<INode> unsettledNodes = new HashSet<>();

        unsettledNodes.add(source);

        while (unsettledNodes.size() != 0) {
            INode currentNode = getLowestDistanceNode(unsettledNodes);
            unsettledNodes.remove(currentNode);
            for (Map.Entry<INode, Double> adjacencyPair:
                    currentNode.getAdjacentNodes().entrySet()) {
                INode adjacentNode = adjacencyPair.getKey();
                double edgeWeight = adjacencyPair.getValue();
                if (!settledNodes.contains(adjacentNode)) {
                    calculateMinimumDistance(adjacentNode, edgeWeight, currentNode);
                    unsettledNodes.add(adjacentNode);
                }
            }
            settledNodes.add(currentNode);
        }
        return IGraph;
    }

    private INode getLowestDistanceNode(Set <INode> unsettledNodes) {
        INode lowestDistanceNode = null;
        double lowestDistance = Double.MAX_VALUE;
        for (INode node: unsettledNodes) {
            double nodeDistance = node.getDistance();
            if (nodeDistance < lowestDistance) {
                lowestDistance = nodeDistance;
                lowestDistanceNode = node;
            }
        }
        return lowestDistanceNode;
    }

    private void calculateMinimumDistance(INode evaluationNode, Double edgeWeigh, INode sourceNode) {
        Double sourceDistance = sourceNode.getDistance();
        if (sourceDistance + edgeWeigh < evaluationNode.getDistance()) {
            evaluationNode.setDistance(sourceDistance + edgeWeigh);
            LinkedList<INode> shortestPath = new LinkedList<>(sourceNode.getShortestPath());
            shortestPath.add(sourceNode);
            evaluationNode.setShortestPath(shortestPath);
        }
    }
}
