package com.htec.flightadvisor.travel.service;

import com.htec.flightadvisor.travel.model.IGraph;
import com.htec.flightadvisor.travel.model.INode;

public interface PathFindingAlgorithm {

    IGraph findPath(IGraph IGraph, INode source);
}
