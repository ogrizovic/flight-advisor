package com.htec.flightadvisor.travel.service;

import java.util.List;

import com.htec.flightadvisor.travel.model.IEdge;
import com.htec.flightadvisor.travel.model.INode;

public interface RoutingService {

    List<INode> findRoutesFromSource(Integer source, List<Integer> destinations, List<INode> nodes, List<IEdge> IEdges);
}
