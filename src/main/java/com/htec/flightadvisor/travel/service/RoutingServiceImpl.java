package com.htec.flightadvisor.travel.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.SerializationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.htec.flightadvisor.travel.model.Graph;
import com.htec.flightadvisor.travel.model.IEdge;
import com.htec.flightadvisor.travel.model.IGraph;
import com.htec.flightadvisor.travel.model.INode;
import com.htec.flightadvisor.travel.model.Pair;

@Service
public class RoutingServiceImpl implements RoutingService {

    private final PathFindingAlgorithm algorithm;

    @Autowired
    public RoutingServiceImpl(PathFindingAlgorithm algorithm) {
        this.algorithm = algorithm;
    }

    public List<INode> findRoutesFromSource(Integer source, List<Integer> destinations, List<INode> nodes, List<IEdge> IEdges) {

        final IGraph IGraph = setupAndCalculateGraph(source, nodes, IEdges);

        return getDestinationNodesFromGraph(destinations, IGraph);

    }

    private IGraph setupAndCalculateGraph(Integer sourceId, List<INode> allNodes, List<IEdge> allIEdges) {
        Pair<IGraph, List<INode>> graphAndNodes = setupGraphAndNodes(allNodes, allIEdges);
        IGraph IGraph = graphAndNodes.getKey();
        List<INode> nodes = graphAndNodes.getValue();

        Optional<INode> sourceNode = nodes.stream()
                .filter(n -> n.getName().equals(sourceId))
                .findFirst();

        if (sourceNode.isPresent()) {
            IGraph = algorithm.findPath(IGraph, sourceNode.get());
        }

        return IGraph;
    }

    private List<INode> getDestinationNodesFromGraph(List<Integer> destinationAirportsIds, IGraph IGraph) {
        return IGraph.getNodes().stream()
                .filter(node ->  destinationAirportsIds.contains(node.getName()))
                .map(SerializationUtils::clone)
                .collect(Collectors.toList());
    }

    private Pair<IGraph, List<INode>> setupGraphAndNodes(List<INode> nodes, List<IEdge> IEdges) {
        Map<Integer, List<IEdge>> edgesMap = generateEdgesMap(IEdges);

        IGraph IGraph = new Graph();
        List<IEdge> currentNodeIEdges;

        for (INode node : nodes) {
            currentNodeIEdges = edgesMap.get(node.getName());
            if (currentNodeIEdges != null) {
                for (IEdge IEdge : currentNodeIEdges) {

                    Optional<INode> destinationNode = nodes.stream()
                            .filter(n -> n.getName().equals(IEdge.getDestination()))
                            .findFirst();

                    destinationNode.ifPresent(dn -> node.addDestination(dn, IEdge.getCost()));
                }
            }
            IGraph.addNode(node);
        }

        return new Pair<>(IGraph, nodes);
    }

    private Map<Integer, List<IEdge>> generateEdgesMap(List<IEdge> IEdges) {
        Map<Integer, List<IEdge>> edgesMap = new HashMap<>();

        for (IEdge IEdge : IEdges) {
            if (edgesMap.containsKey(IEdge.getSource())) {
                continue;
            }

            List<IEdge> destinations = IEdges.stream()
                    .filter(e -> e.getSource().equals(IEdge.getSource()))
                    .collect(Collectors.toList());

            List<IEdge> cheapestOfDuplicates = getCheapestOfDuplicates(destinations);

            edgesMap.put(IEdge.getSource(), cheapestOfDuplicates);
        }

        return edgesMap;
    }

    private List<IEdge> getCheapestOfDuplicates(List<IEdge> IEdges) {
        final Map<Integer, IEdge> cheapestOfDuplicates = new HashMap<>();

        for (IEdge IEdge : IEdges) {
            if (!cheapestOfDuplicates.containsKey(IEdge.getDestination())) {
                cheapestOfDuplicates.put(IEdge.getDestination(), IEdge);
                continue;
            }

            final Double storedDestinationCost = cheapestOfDuplicates.get(IEdge.getDestination()).getCost();
            if (IEdge.getCost() < storedDestinationCost) {
                cheapestOfDuplicates.put(IEdge.getDestination(), IEdge);
            }
        }

        return new ArrayList<>(cheapestOfDuplicates.values());
    }
}
