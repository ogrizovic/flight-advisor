package com.htec.flightadvisor.travel.service;

import com.htec.flightadvisor.travel.model.TravelDto;

public interface TravelService {

    TravelDto travel(String source, String destination);
}
