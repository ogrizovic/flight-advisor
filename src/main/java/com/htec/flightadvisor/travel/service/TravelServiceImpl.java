package com.htec.flightadvisor.travel.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.htec.flightadvisor.importdata.model.Airport;
import com.htec.flightadvisor.importdata.model.Route;
import com.htec.flightadvisor.importdata.service.AirportService;
import com.htec.flightadvisor.importdata.service.RouteService;
import com.htec.flightadvisor.travel.exception.NoAirportsFound;
import com.htec.flightadvisor.travel.model.Edge;
import com.htec.flightadvisor.travel.model.IEdge;
import com.htec.flightadvisor.travel.model.INode;
import com.htec.flightadvisor.travel.model.Node;
import com.htec.flightadvisor.travel.model.RouteDto;
import com.htec.flightadvisor.travel.model.TravelDto;

@Service
public class TravelServiceImpl implements TravelService {

    private final AirportService airportService;
    private final RouteService routeService;
    private final RoutingService routingService;

    @Autowired
    public TravelServiceImpl(AirportService airportService, RouteService routeService, RoutingServiceImpl routingService) {
        this.airportService = airportService;
        this.routeService = routeService;
        this.routingService = routingService;
    }

    @Override
    public TravelDto travel(String sourceCity, String destinationCity) {
        final List<Airport> allAirports = airportService.getAll();
        final List<Route> allRoutes = routeService.getAll();

        final List<Integer> sourceCityAirportsIds = getAirportIdsForCity(allAirports, sourceCity);
        final List<Integer> destinationCityAirportsIds = getAirportIdsForCity(allAirports, destinationCity);

        if (sourceCityAirportsIds.isEmpty()) {
            throw new NoAirportsFound("No airports found for city " + sourceCity);
        }

        if (destinationCityAirportsIds.isEmpty()) {
            throw new NoAirportsFound("No airports found for city " + destinationCity);
        }

        final Map<Integer, List<INode>> pathsFromEverySourceAirport = new HashMap<>();

        final List<IEdge> allEdges = generateEdgesFromRoutes(allRoutes);

        for (Integer sourceAirportId : sourceCityAirportsIds) {

                final List<INode> destinationNodes = routingService.findRoutesFromSource(
                    sourceAirportId,
                    destinationCityAirportsIds,
                    generateCleanNodesFromAirports(allAirports),
                    allEdges);

            pathsFromEverySourceAirport.put(sourceAirportId, destinationNodes);
        }

        INode cheapestDestinationNode = getTheCheapestDestinationNode(pathsFromEverySourceAirport);
        List<Route> routes = convertToRoutes(cheapestDestinationNode, allRoutes);
        return getTravelDto(routes, allAirports);
    }

    private List<IEdge> generateEdgesFromRoutes(List<Route> routes) {
        return routes.stream().map(Edge::new).collect(Collectors.toList());
    }

    private List<INode> generateCleanNodesFromAirports(List<Airport> allAirports) {
        return allAirports.stream()
                .map(airport -> new Node(airport.getAirportId()))
                .collect(Collectors.toList());
    }

    private INode getTheCheapestDestinationNode(Map<Integer, List<INode>> sourceToDestinations) {
        INode cheapestDestination = null;
        for (Integer srcAirport : sourceToDestinations.keySet()) {
            final List<INode> destinations = sourceToDestinations.get(srcAirport);
            for (INode node : destinations) {
                if (cheapestDestination == null) {
                    cheapestDestination = node;
                } else if (node.getDistance() < cheapestDestination.getDistance()) {
                    cheapestDestination = node;
                }
            }
        }
        return cheapestDestination;
    }

    private List<Route> convertToRoutes(INode node, List<Route> allRoutes) {
        List<Route> routes = new ArrayList<>();
        if (node == null) {
            return routes;
        }

        final Integer finalDest = node.getName();
        final double finalPrice = node.getDistance();
        final List<INode> shortestPath = node.getShortestPath();

        for (int i = 0; i < shortestPath.size(); i++) {

            int src;
            int dest;
            double price;

            if ((i + 1) == shortestPath.size()) {
                src = shortestPath.get(i).getName();
                dest = finalDest;
                price = finalPrice - shortestPath.get(i).getDistance();
            } else {
                src = shortestPath.get(i).getName();
                dest = shortestPath.get(i + 1).getName();
                price = shortestPath.get(i + 1).getDistance() - shortestPath.get(i).getDistance();
            }
//            routes.add(routeService.getBySourceDestPrice(src, dest, truncateTo2Decimals(price)));
            final double truncatedPrice = truncateTo2Decimals(price);

            allRoutes.stream()
                    .filter(r -> src == r.getSourceAirportId() &&
                            dest == r.getDestinationAirportId() &&
                            truncatedPrice == r.getPrice())
                    .findFirst()
                    .ifPresent(routes::add);

        }

        return routes;
    }

    private TravelDto getTravelDto(List<Route> routes, List<Airport> airports) {
        final List<RouteDto> routesDto = new ArrayList<>();
        double totalPrice = 0.0;

        for (Route route : routes) {
            final RouteDto dto = new RouteDto();
            final Optional<Airport> sourceAirport = airports.stream()
                    .filter(airport -> airport.getAirportId().equals(route.getSourceAirportId()))
                    .findFirst();
            final Optional<Airport> destinationAirport = airports.stream()
                    .filter(airport -> airport.getAirportId().equals(route.getDestinationAirportId()))
                    .findFirst();

            sourceAirport.ifPresent(airport -> dto.setSourceCityName(airport.getCityName()));
            destinationAirport.ifPresent(airport -> dto.setDestinationCityName(airport.getCityName()));

            dto.setPrice(route.getPrice());
            totalPrice += route.getPrice();

            routesDto.add(dto);
        }

        return new TravelDto(routesDto, truncateTo2Decimals(totalPrice));
    }

    private double truncateTo2Decimals(double value) {
        return Math.floor(value * 100) / 100;
    }

    private List<Integer> getAirportIdsForCity(List<Airport> airports, String cityName) {
        return airports.stream()
                .filter(airport -> airport.getCityName().equals(cityName))
                .map(Airport::getAirportId)
                .collect(Collectors.toList());
    }
}
