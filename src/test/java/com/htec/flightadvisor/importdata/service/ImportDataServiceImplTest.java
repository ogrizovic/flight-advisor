package com.htec.flightadvisor.importdata.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.htec.flightadvisor.city.service.CityService;
import com.htec.flightadvisor.importdata.model.Airport;
import com.htec.flightadvisor.importdata.model.Route;

import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.mockito.Mockito.anyList;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ImportDataServiceImplTest {

    private List<String> ADDED_CITIES = new ArrayList<>();

    @Mock
    private CityService cityService;
    @Mock
    private AirportService airportService;
    @Mock
    private RouteService routeService;
    @InjectMocks
    private ImportDataServiceImpl importDataService;
    @Captor
    private ArgumentCaptor<List<Airport>> addAirportsCaptor;
    @Captor
    private ArgumentCaptor<List<Route>> addRoutesCaptor;

    @BeforeEach
    void setUp() {
        ADDED_CITIES.clear();
        ADDED_CITIES.add("City 1");
        ADDED_CITIES.add("City 2");
        ADDED_CITIES.add("City 3");
    }

    @Test
    void importData() {
        List<Airport> expectedAirports = getExpectedAirports();
        List<Route> expectedRoutes = getExpectedRoutes();

        when(cityService.getAddedCityNames()).thenReturn(ADDED_CITIES);
        when(airportService.addAll(anyList())).thenReturn(Collections.emptyList());
        when(routeService.addAll(anyList())).thenReturn(Collections.emptyList());

        importDataService.importData();

        verify(airportService).addAll(addAirportsCaptor.capture());
        verify(routeService).addAll(addRoutesCaptor.capture());

        List<Airport> actualAirports = addAirportsCaptor.getValue();
        List<Route> actualRoutes = addRoutesCaptor.getValue();

        assertIterableEquals(expectedAirports, actualAirports);
        assertIterableEquals(expectedRoutes, actualRoutes);
    }

    private List<Route> getExpectedRoutes() {
        List<Route> routes = new ArrayList<>();

        Route r1 = new Route(new String[]{"AA","24","LAX","1","SJC","2","Y","0","CR7 CRJ","10.00"});
        Route r2 = new Route(new String[]{"AA","24","LAX","2","SJC","3","Y","0","CR7 CRJ","5.00"});
        Route r3 = new Route(new String[]{"AA","24","LAX","12","SJC","3","Y","0","CR7 CRJ","12.00"});

        routes.add(r1);
        routes.add(r2);
        routes.add(r3);

        return routes;
    }

    private List<Airport> getExpectedAirports() {
        List<Airport> airports = new ArrayList<>();

        Airport a1 = new Airport(new String[]{"1","Cincinnati Municipal Airport Lunken Field","City 1","United States","LUK","KLUK","39.10329819","-84.41860199","483","-5","A","America/New_York","airport","OurAirports"});
        Airport a2 = new Airport(new String[]{"2","Cincinnati Municipal Airport Lunken Field","City 2","United States","LUK","KLUK","39.10329819","-84.41860199","483","-5","A","America/New_York","airport","OurAirports"});
        Airport a3 = new Airport(new String[]{"3","Cincinnati Municipal Airport Lunken Field","City 3","United States","LUK","KLUK","39.10329819","-84.41860199","483","-5","A","America/New_York","airport","OurAirports"});
        Airport a4 = new Airport(new String[]{"11","Cincinnati Municipal Airport Lunken Field","City 1","United States","LUK","KLUK","39.10329819","-84.41860199","483","-5","A","America/New_York","airport","OurAirports"});
        Airport a5 = new Airport(new String[]{"12","Cincinnati Municipal Airport Lunken Field","City 1","United States","LUK","KLUK","39.10329819","-84.41860199","483","-5","A","America/New_York","airport","OurAirports"});
        Airport a6 = new Airport(new String[]{"33","Cincinnati Municipal Airport Lunken Field","City 3","United States","LUK","KLUK","39.10329819","-84.41860199","483","-5","A","America/New_York","airport","OurAirports"});

        airports.add(a1);
        airports.add(a2);
        airports.add(a3);
        airports.add(a4);
        airports.add(a5);
        airports.add(a6);

        return airports;
    }
}