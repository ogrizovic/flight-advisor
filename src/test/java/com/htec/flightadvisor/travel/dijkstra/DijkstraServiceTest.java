package com.htec.flightadvisor.travel.dijkstra;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.htec.flightadvisor.travel.service.DijkstraService;
import com.htec.flightadvisor.travel.model.Graph;
import com.htec.flightadvisor.travel.model.IGraph;
import com.htec.flightadvisor.travel.model.INode;
import com.htec.flightadvisor.travel.model.Node;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DijkstraServiceTest {

    private final DijkstraService dijkstraService = new DijkstraService();

    @Test
    void calculateCheapestPath() {
        Node node1 = new Node(1);
        Node node2 = new Node(2);
        Node node3 = new Node(3);
        Node node4 = new Node(4);
        Node node5 = new Node(5);
        Node node6 = new Node(6);

        node1.addDestination(node2, 10);
        node1.addDestination(node3, 15);

        node2.addDestination(node4, 12);
        node2.addDestination(node6, 15);

        node3.addDestination(node5, 10);

        node4.addDestination(node5, 2);
        node4.addDestination(node6, 1);

        node6.addDestination(node5, 5);

        IGraph IGraph = new Graph();

        IGraph.addNode(node1);
        IGraph.addNode(node2);
        IGraph.addNode(node3);
        IGraph.addNode(node4);
        IGraph.addNode(node5);
        IGraph.addNode(node6);

        IGraph = dijkstraService.findPath(IGraph, node1);

        List<Node> shortestPathForNodeB = Arrays.asList(node1);
        List<Node> shortestPathForNodeC = Arrays.asList(node1);
        List<Node> shortestPathForNodeD = Arrays.asList(node1, node2);
        List<Node> shortestPathForNodeE = Arrays.asList(node1, node2, node4);
        List<Node> shortestPathForNodeF = Arrays.asList(node1, node2, node4);

        for (INode node : IGraph.getNodes()) {
            switch (node.getName()) {
                case 2:
                    assertEquals(node.getShortestPath(), shortestPathForNodeB);
                    break;
                case 3:
                    assertEquals(node.getShortestPath(), shortestPathForNodeC);
                    break;
                case 4:
                    assertEquals(node.getShortestPath(), shortestPathForNodeD);
                    break;
                case 5:
                    assertEquals(node.getShortestPath(), shortestPathForNodeE);
                    break;
                case 6:
                    assertEquals(node.getShortestPath(), shortestPathForNodeF);
                    break;
            }
        }
    }
}