package com.htec.flightadvisor.travel.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.htec.flightadvisor.importdata.model.Route;
import com.htec.flightadvisor.travel.model.Edge;
import com.htec.flightadvisor.travel.model.Graph;
import com.htec.flightadvisor.travel.model.IEdge;
import com.htec.flightadvisor.travel.model.IGraph;
import com.htec.flightadvisor.travel.model.INode;
import com.htec.flightadvisor.travel.model.Node;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RoutingServiceImplTest {

    private final List<Route> ALL_ROUTES = getAllRoutes();
    private final int[] NODE_IDs = new int[] {1, 2, 3, 11, 12, 33};

    @Mock
    DijkstraService dijkstraService;

    @InjectMocks
    RoutingServiceImpl routingService;

    @Test
    void findRoutes_1to2_findsOneDestination() {
        Node sourceNode = new Node(1);
        sourceNode.setDistance(Double.MAX_VALUE);
        final List<Integer> destinations = Collections.singletonList(2);


        when(dijkstraService.findPath(any(), eq(sourceNode)))
                .thenReturn(getGraph_source1());

        List<INode> actual = routingService.findRoutesFromSource(1, destinations, getFreshNodes(), getEdges());


        Node nodeResult = new Node(2);
        nodeResult.setDistance(10.0);

        Node nodeInPath = new Node(1);
        nodeInPath.setDistance(0.00);

        nodeResult.setShortestPath(Collections.singletonList(nodeInPath));

        List<Node> expected = Collections.singletonList(nodeResult);

        assertEquals(expected, actual);
    }

    @Test
    void findRoutes_1to3_findsTwoDestinations() {
        Node sourceNode = new Node(1);
        sourceNode.setDistance(Double.MAX_VALUE);
        final List<Integer> destinations = Arrays.asList(3, 33);


        when(dijkstraService.findPath(any(), eq(sourceNode)))
                .thenReturn(getGraph_source1());

        List<INode> actual = routingService.findRoutesFromSource(1, destinations, getFreshNodes(), getEdges());


        Node nodeResult1 = new Node(3);
        nodeResult1.setDistance(15.0);

        Node nodeInPath1 = new Node(1);
        nodeInPath1.setDistance(0.00);

        Node nodeInPath2 = new Node(2);
        nodeInPath2.setDistance(10.00);

        nodeResult1.setShortestPath(Collections.singletonList(nodeInPath1));

        Node nodeResult2 = new Node(33);
        nodeResult2.setDistance(Double.MAX_VALUE);

        List<Node> expected = Arrays.asList(nodeResult1, nodeResult2);

        assertTrue(expected.containsAll(actual) && actual.containsAll(expected));
    }

    @Test
    void findRoutes_3to2_findsNothing() {
        Node sourceNode = new Node(3);
        sourceNode.setDistance(Double.MAX_VALUE);
        final List<Integer> destinations = Collections.singletonList(2);


        when(dijkstraService.findPath(any(), eq(sourceNode)))
                .thenReturn(getGraph_source3());

        List<INode> actual = routingService.findRoutesFromSource(3, destinations, getFreshNodes(), getEdges());


        Node nodeResult = new Node(2);
        nodeResult.setDistance(Double.MAX_VALUE);

        List<Node> expected = Collections.singletonList(nodeResult);

        assertEquals(expected, actual);
    }

    private IGraph getGraph_source3() {
        IGraph IGraph = new Graph();
        Node n1 = new Node(1);
        n1.setDistance(Double.MAX_VALUE);
        Node n2 = new Node(2);
        n2.setDistance(Double.MAX_VALUE);
        Node n3 = new Node(3);
        n3.setDistance(0.00);
        Node n11 = new Node(11);
        n11.setDistance(Double.MAX_VALUE);
        Node n12 = new Node(12);
        n12.setDistance(Double.MAX_VALUE);
        Node n33 = new Node(33);
        n33.setDistance(Double.MAX_VALUE);

        IGraph.setNodes(Set.of(n1, n2, n3, n11, n12, n33));

        return IGraph;
    }

    private IGraph getGraph_source1() {
        IGraph IGraph = new Graph();
        Node n1 = new Node(1);
        n1.setDistance(0.0);
        Node n2 = new Node(2);
        n2.setDistance(10.0);
        Node n3 = new Node(3);
        n3.setDistance(15.00);
        Node n11 = new Node(11);
        n11.setDistance(Double.MAX_VALUE);
        Node n12 = new Node(12);
        n12.setDistance(Double.MAX_VALUE);
        Node n33 = new Node(33);
        n33.setDistance(Double.MAX_VALUE);

        IGraph.setNodes(Set.of(n1, n2, n3, n11, n12, n33));

        return IGraph;
    }

    private IGraph getFreshGraph() {
        IGraph IGraph = new Graph();
        IGraph.setNodes(new HashSet<>(getFreshNodes()));
        return IGraph;
    }

    private List<INode> getFreshNodes() {
        List<INode> nodes = new ArrayList<>();
        for (int nodeId : NODE_IDs) {
            Node n = new Node(nodeId);
            n.setDistance(Double.MAX_VALUE);
            nodes.add(n);
        }
        return nodes;
    }

    private List<IEdge> getEdges() {
        return ALL_ROUTES.stream().map(Edge::new).collect(Collectors.toList());
    }

    private List<Route> getAllRoutes() {
        Route r1 = new Route(new String[]{"", "", "", "1", "", "2", "", "", "", "10.00"});
        Route r2 = new Route(new String[]{"", "", "", "2", "", "3", "", "", "", "5.00"});
        Route r3 = new Route(new String[]{"", "", "", "12", "", "3", "", "", "", "12.00"});

        return new ArrayList<>(Arrays.asList(r1, r2, r3));
    }
}