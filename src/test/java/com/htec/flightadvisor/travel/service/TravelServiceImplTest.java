package com.htec.flightadvisor.travel.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.htec.flightadvisor.importdata.model.Airport;
import com.htec.flightadvisor.importdata.model.Route;
import com.htec.flightadvisor.importdata.service.AirportService;
import com.htec.flightadvisor.importdata.service.RouteService;
import com.htec.flightadvisor.travel.exception.NoAirportsFound;
import com.htec.flightadvisor.travel.model.INode;
import com.htec.flightadvisor.travel.model.Node;
import com.htec.flightadvisor.travel.model.RouteDto;
import com.htec.flightadvisor.travel.model.TravelDto;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

//@SpringBootTest
@ExtendWith(MockitoExtension.class)
class TravelServiceImplTest {

    private final List<Airport> ALL_AIRPORTS = getAllAirports();
    private final List<Route> ALL_ROUTES = getAllRoutes();

    @Mock
    AirportService airportService;

    @Mock
    RouteService routeService;

    @Mock
    RoutingServiceImpl routingService;

    @InjectMocks
    TravelServiceImpl travelService;

    @Test
    void travel_whenNoAirports_throwException() {
        when(airportService.getAll()).thenReturn(ALL_AIRPORTS);
        when(routeService.getAll()).thenReturn(ALL_ROUTES);

        assertThrows(NoAirportsFound.class, () -> travelService.travel("xxx", "yyy"));

    }

    @Test
    void travel_1to2_success() {
        when(airportService.getAll()).thenReturn(ALL_AIRPORTS);
        when(routeService.getAll()).thenReturn(ALL_ROUTES);
        when(routingService.findRoutesFromSource(eq(1), anyList(), anyList(), anyList()))
                .thenReturn(getDestNodes_1to2_Source1());
        when(routingService.findRoutesFromSource(eq(11), anyList(), anyList(), anyList()))
                .thenReturn(getDestNodes_1to2_Source11());
        when(routingService.findRoutesFromSource(eq(12), anyList(), anyList(), anyList()))
                .thenReturn(getDestNodes_1to2_Source12());

        TravelDto actual = travelService.travel("City 1", "City 2");
        TravelDto expected = getTravelDto_1to2();

        assertEquals(expected, actual);

    }

    @Test
    void travel_1to3_success() {
        when(airportService.getAll()).thenReturn(ALL_AIRPORTS);
        when(routeService.getAll()).thenReturn(ALL_ROUTES);
        when(routingService.findRoutesFromSource(eq(1), anyList(), anyList(), anyList()))
                .thenReturn(getDestNodes_1to3_Source1());
        when(routingService.findRoutesFromSource(eq(11), anyList(), anyList(), anyList()))
                .thenReturn(getDestNodes_1to3_Source11());
        when(routingService.findRoutesFromSource(eq(12), anyList(), anyList(), anyList()))
                .thenReturn(getDestNodes_1to3_Source12());

        TravelDto actual = travelService.travel("City 1", "City 3");
        TravelDto expected = getTravelDto_1to3();

        assertEquals(expected, actual);

    }

    @Test
    void travel_3to1_noRoutes() {
        when(airportService.getAll()).thenReturn(ALL_AIRPORTS);
        when(routeService.getAll()).thenReturn(ALL_ROUTES);
        when(routingService.findRoutesFromSource(eq(3), anyList(), anyList(), anyList()))
                .thenReturn(getDestNodes_3to1_Source3and33());
        when(routingService.findRoutesFromSource(eq(33), anyList(), anyList(), anyList()))
                .thenReturn(getDestNodes_3to1_Source3and33());

        TravelDto actual = travelService.travel("City 3", "City 1");
        TravelDto expected = new TravelDto(Collections.emptyList(), 0.0);

        assertEquals(expected, actual);

    }

    private List<INode> getDestNodes_3to1_Source3and33() {
        Node n1 = new Node(1);
        n1.setDistance(Double.MAX_VALUE);
        Node n11 = new Node(11);
        n11.setDistance(Double.MAX_VALUE);
        Node n12 = new Node(12);
        n12.setDistance(Double.MAX_VALUE);

        return Arrays.asList(n1, n11, n12);
    }

    private TravelDto getTravelDto_1to3() {
        RouteDto r1 = new RouteDto("City 1", "City 5", 1.0);
        RouteDto r2 = new RouteDto("City 5", "City 3", 1.0);
        List<RouteDto> routes = Arrays.asList(r1, r2);
        double totalPrice = 2.0;

        return new TravelDto(routes, totalPrice);
    }

    private List<INode> getDestNodes_1to3_Source1() {
        Node n3 = new Node(3);
        Node n1 = new Node(1);
        n1.setDistance(0.0);
        Node n4 = new Node(4);
        n4.setDistance(1.0);
        Node n5 = new Node(5);
        n5.setDistance(3.0);
        Node n33 = new Node(33);

        n3.getShortestPath().addAll(Arrays.asList(n1, n4, n5));
        n3.setDistance(6.0);

        n33.getShortestPath().addAll(Arrays.asList(n1, n4, n5));
        n33.setDistance(4.0);

        return Arrays.asList(n3, n33);
    }

    private List<INode> getDestNodes_1to3_Source12() {
        Node n3 = new Node(3);
        Node n12 = new Node(12);
        n12.setDistance(0.0);
        Node n5 = new Node(5);
        n5.setDistance(1.0);
        Node n33 = new Node(33);

        n33.getShortestPath().addAll(Arrays.asList(n12, n5));
        n33.setDistance(2.0);

        n3.getShortestPath().addAll(Arrays.asList(n12, n5));
        n3.setDistance(4.0);

        return Arrays.asList(n3, n33);
    }

    private List<INode> getDestNodes_1to3_Source11() {
        Node n3 = new Node(3);
        n3.setDistance(Double.MAX_VALUE);
        Node n33 = new Node(33);
        n33.setDistance(Double.MAX_VALUE);

        return Arrays.asList(n3, n33);
    }

    private TravelDto getTravelDto_1to2() {
        RouteDto r1 = new RouteDto("City 1", "City 2", 10.0);
        List<RouteDto> routes = Collections.singletonList(r1);
        double totalPrice = 10.0;

        return new TravelDto(routes, totalPrice);
    }

    private List<INode> getDestNodes_1to2_Source1() {
        Node n2 = new Node(2);
        n2.setDistance(10.0);
        Node n1 = new Node(1);
        n1.setDistance(0.0);

        n2.getShortestPath().add(n1);

        return Collections.singletonList(n2);
    }

    private List<INode> getDestNodes_1to2_Source12() {
        Node n2 = new Node(2);
        n2.setDistance(Double.MAX_VALUE);

        return Collections.singletonList(n2);
    }

    private List<INode> getDestNodes_1to2_Source11() {
        Node n2 = new Node(2);
        n2.setDistance(Double.MAX_VALUE);

        return Collections.singletonList(n2);
    }

    private List<Airport> getAllAirports() {
        Airport a1 = new Airport(new String[]{"1", "", "City 1", "", "", "", "", "", "", "", "", "", "", ""});
        Airport a2 = new Airport(new String[]{"2", "", "City 2", "", "", "", "", "", "", "", "", "", "", ""});
        Airport a3 = new Airport(new String[]{"3", "", "City 3", "", "", "", "", "", "", "", "", "", "", ""});
        Airport a4 = new Airport(new String[]{"4", "", "City 4", "", "", "", "", "", "", "", "", "", "", ""});
        Airport a5 = new Airport(new String[]{"5", "", "City 5", "", "", "", "", "", "", "", "", "", "", ""});
        Airport a11 = new Airport(new String[]{"11", "", "City 1", "", "", "", "", "", "", "", "", "", "", ""});
        Airport a12 = new Airport(new String[]{"12", "", "City 1", "", "", "", "", "", "", "", "", "", "", ""});
        Airport a33 = new Airport(new String[]{"33", "", "City 3", "", "", "", "", "", "", "", "", "", "", ""});

        return new ArrayList<>(Arrays.asList(a1, a2, a3, a4, a5, a11, a12, a33));
    }

    private List<Route> getAllRoutes() {
        Route r1 = new Route(new String[]{"", "", "", "1", "", "2", "", "", "", "10.00"});
        Route r2 = new Route(new String[]{"", "", "", "2", "", "3", "", "", "", "5.00"});
        Route r3 = new Route(new String[]{"", "", "", "1", "", "4", "", "", "", "1.00"});
        Route r4 = new Route(new String[]{"", "", "", "4", "", "5", "", "", "", "2.00"});
        Route r5 = new Route(new String[]{"", "", "", "5", "", "3", "", "", "", "3.00"});
        Route r6 = new Route(new String[]{"", "", "", "12", "", "5", "", "", "", "1.00"});
        Route r7 = new Route(new String[]{"", "", "", "12", "", "3", "", "", "", "12.00"});
        Route r8 = new Route(new String[]{"", "", "", "5", "", "33", "", "", "", "1.00"});
        Route r9 = new Route(new String[]{"", "", "", "5", "", "11", "", "", "", "2.00"});

        return new ArrayList<>(Arrays.asList(r1, r2, r3, r4, r5, r6, r7, r8, r9));
    }

}